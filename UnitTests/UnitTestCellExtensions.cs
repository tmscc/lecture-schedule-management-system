﻿using DocumentFormat.OpenXml.Spreadsheet;
using LectureScheduleManagementSystem.Extensions;
using LectureScheduleManagementSystem.Helpers;
using Xunit;

namespace UnitTests
{
	public class UnitTestCellExtensions
	{
		[Fact]
		public void TestIsNull()
		{
			Cell cell = new Cell();
			
			Assert.True(!cell.IsNull());
		}
	}
}
