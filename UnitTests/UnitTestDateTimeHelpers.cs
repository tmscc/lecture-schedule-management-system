﻿using LectureScheduleManagementSystem.Helpers;
using Xunit;

namespace UnitTests
{
	public class UnitTestDateTimeHelpers
	{
		[Fact]
		public void TestGetLongDateTime()
		{
			string result = DateTimeHelpers.GetDateLong("27 Feb 2020");

			Assert.Contains("February", result);
		}

		[Fact]
		public void TestGetShortDateTime()
		{
			string result = DateTimeHelpers.GetDateShort("27 February 2020");

			Assert.Contains(".02.", result);
		}
	}
}
