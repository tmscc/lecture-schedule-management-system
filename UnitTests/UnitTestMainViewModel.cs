﻿using LectureScheduleManagementSystem.ViewModels;
using System;
using Xunit;

namespace UnitTests
{
	public class UnitTestMainViewModel
	{
		[Fact]
		public void TestCommandCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.CommandCanExecute());
		}

		[Fact]
		public void TestIsBusy()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.IsBusy);
		}

		[Fact]
		public void TestImportCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.True(mainViewModel.ImportCommand.CanExecute(""));
		}

		[Fact]
		public void TestExportCsvCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.ExportCsvCommand.CanExecute(""));
		}

		[Fact]
		public void TestExportExcelXlsCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.ExportExcelXlsCommand.CanExecute(""));
		}

		[Fact]
		public void TestExportExcelXlsxCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.ExportExcelXlsxCommand.CanExecute(""));
		}

		[Fact]
		public void TestExportHtmlCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.ExportHtmlCommand.CanExecute(""));
		}

		[Fact]
		public void TestExportExcelHtmlCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.ExportXmlCommand.CanExecute(""));
		}


		[Fact]
		public void TestExportJsonCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.ExportJsonCommand.CanExecute(""));

			mainViewModel.TreeViewItems.Add(new Parent(""));

			Assert.True(mainViewModel.ExportJsonCommand.CanExecute(""));
		}

		[Fact]
		public void TestClearCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.ClearCommand.CanExecute(""));

			mainViewModel.TreeViewItems.Add(new Parent(""));

			Assert.True(mainViewModel.ClearCommand.CanExecute(""));
		}

		[Fact]
		public void TestSaveCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.SaveCommand.CanExecute(""));

			mainViewModel.TreeViewItems.Add(new Parent(""));

			Assert.True(mainViewModel.SaveCommand.CanExecute(""));
		}

		[Fact]
		public void TestAddCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.OpenAddCommand.CanExecute(""));

			mainViewModel.TreeViewItems.Add(new Parent(""));

			Assert.True(mainViewModel.OpenAddCommand.CanExecute(""));
		}

		[Fact]
		public void TestEditCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.OpenEditCommand.CanExecute(""));

			mainViewModel.TreeViewItems.Add(new Parent(""));

			Assert.True(mainViewModel.OpenEditCommand.CanExecute(""));
		}

		[Fact]
		public void TestDeleteCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.OpenDeleteCommand.CanExecute(""));

			mainViewModel.TreeViewItems.Add(new Parent(""));

			Assert.True(mainViewModel.OpenDeleteCommand.CanExecute(""));
		}

		[Fact]
		public void TestOpenVumCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.True(mainViewModel.OpenVumCommand.CanExecute(""));
		}

		[Fact]
		public void TestPrintCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.False(mainViewModel.PrintCommand.CanExecute(""));

			mainViewModel.TreeViewItems.Add(new Parent(""));

			Assert.True(mainViewModel.PrintCommand.CanExecute(""));
		}

		[Fact]
		public void TestOpenLocationCanExecute()
		{
			var mainViewModel = new MainViewModel();

			Assert.True(mainViewModel.OpenLocationCommand.CanExecute(""));
		}
	}
}
