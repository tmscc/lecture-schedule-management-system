﻿using LectureScheduleManagementSystem.Managers;
using LectureScheduleManagementSystem.Models;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace UnitTests
{
	public class UnitTestHtmlFileWriter
	{
		[Fact]
		public void TestFileGeneratedCompact()
		{
			var mock = new Mock<List<ExcelColumn>>();
			var generator = new HtmlCompactGenerator();

			string result = generator.Generate(mock.Object, "");

			Assert.Contains("html", result);
			Assert.False(string.IsNullOrEmpty(result));
			Assert.Contains("body", result);
			Assert.Contains("head", result);
		}


		[Fact]
		public void TestFileGeneratedNonCompact()
		{
		}
	}
}
