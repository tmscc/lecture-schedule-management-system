﻿using LectureScheduleManagementSystem.Helpers;
using LectureScheduleManagementSystem.Logging;
using Moq;
using System;
using System.IO;
using System.Net.Http;
using Xunit;

namespace UnitTests
{
	public class UnitTestExceptionHelpers
	{
		[Fact]
		public void TestBuildExceptionNotNullOrEmpty()
		{
			try
			{
				Mock<ILogger> connection = new Mock<ILogger>(MockBehavior.Strict);
				connection.Setup(item => item.LogInfo(""))
							  .Throws(new IOException());
			}
			catch(IOException ex)
			{
				string result = ExceptionHelpers.BuildMessage(ex);
				Assert.True(!string.IsNullOrEmpty(result));
			}
		}

		[Fact]
		public void TestBuildExceptionContains()
		{
			try
			{
				Mock<ILogger> connection = new Mock<ILogger>(MockBehavior.Strict);
				connection.Setup(item => item.LogInfo(""))
							  .Throws(new IOException());
			}
			catch (IOException ex)
			{
				string result = ExceptionHelpers.BuildMessage(ex);
				Assert.Contains("Exception", result);
				Assert.Contains("IO", result);
			}
		}
	}
}
