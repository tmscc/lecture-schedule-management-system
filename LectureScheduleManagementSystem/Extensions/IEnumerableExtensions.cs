﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LectureScheduleManagementSystem.Extensions
{
	/// <summary>
	/// 
	/// </summary>
	public static class IEnumerableExtensions
	{
		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> obj)
			where T : class
		{
			return obj == null || !obj.Any();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <typeparam name="TKey"></typeparam>
		/// <param name="source"></param>
		/// <param name="keySelector"></param>
		/// <returns></returns>
		public static IEnumerable<TSource> DistinctBy<TSource, TKey>(
			this IEnumerable<TSource> source,
			Func<TSource, TKey> keySelector
		)
		{
			var knownKeys = new HashSet<TKey>();
			return source.Where(element => knownKeys.Add(keySelector(element)));
		}
	}
}
