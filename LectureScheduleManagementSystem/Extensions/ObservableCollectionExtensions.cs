﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LectureScheduleManagementSystem.Extensions
{
	public static class ObservableCollectionExtensions
	{
		public static ObservableCollection<T> DistinctBy<T, TKey>(this ObservableCollection<T> items, Func<T, TKey> property)
		{
			return new ObservableCollection<T>(items.GroupBy(property).Select(x => x.First()));
		}
	}
}
