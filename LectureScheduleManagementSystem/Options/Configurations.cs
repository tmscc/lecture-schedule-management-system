﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace LectureScheduleManagementSystem.Options
{
	public static class Configurations
	{
		public static NameValueCollection AppSettings
        {
            get
            {
                NameValueCollection nameValueConfigurationCollection = null;
                try
                {
                    nameValueConfigurationCollection = ConfigurationManager.AppSettings;
                }
                catch (ConfigurationErrorsException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                return nameValueConfigurationCollection;
            }
        }
	}
}
