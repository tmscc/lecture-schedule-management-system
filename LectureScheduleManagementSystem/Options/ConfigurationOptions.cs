﻿using LectureScheduleManagementSystem.Helpers;
using System.Threading.Tasks;

namespace LectureScheduleManagementSystem.Options
{
	/// <summary>
	/// 
	/// </summary>
	static public class ConfigurationOptions
	{
		public static ParallelOptions ParallelOptions 
		{ 
			get 
			{
				return new ParallelOptions { MaxDegreeOfParallelism = int.Parse(Configurations.AppSettings[Constants.KeyMaxDegreeOfParallelism]) };
			} 
		}
	}
}
