﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LectureScheduleManagementSystem.ViewModels
{
    public interface IBaseViewModel : INotifyPropertyChanged
    {
        Action CloseAction { get; set; }
        Action<string, string> ShowPopupMessage { get; set; }
        Func<string, string, bool> ShowConfirmMessage { get; set; }
        Func<string, bool, string, string[]> ShowOpenFileDialog { get; set; }
        Func<Environment.SpecialFolder, bool, string> ShowFolderBrowserDialog { get; set; }
    }
}
