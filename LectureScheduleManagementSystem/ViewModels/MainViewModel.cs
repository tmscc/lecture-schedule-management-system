﻿using LectureScheduleManagementSystem.Parser;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using ExcelColumn = LectureScheduleManagementSystem.Models.ExcelColumn;
using MoreLinq;
using LectureScheduleManagementSystem.Models;
using System;
using System.Windows;
using LectureScheduleManagementSystem.FileFormats;
using System.IO;
using LectureScheduleManagementSystem.Helpers;
using LectureScheduleManagementSystem.Managers;
using IronXL;
using System.ComponentModel;
using System.Drawing.Printing;
using LectureScheduleManagementSystem.Logging;

namespace LectureScheduleManagementSystem.ViewModels
{
	public class DataTemplateSelector : System.Windows.Controls.DataTemplateSelector
	{
		public override DataTemplate
		  SelectTemplate(object item, DependencyObject container)
		{
			FrameworkElement element = container as FrameworkElement;

			return item is Session
			  ? element.FindResource("SessionDataTemplate") as DataTemplate
			  : element.FindResource("FolderDataTemplate") as DataTemplate;
		}
	}
	public abstract class TreeItem : ViewModelBase
	{
		public TreeItem(String name)
		{
			this.Name = name;
		}
		public string Name { get; set; }

		private bool hasPassed;
		public bool HasPassed
		{
			get { return hasPassed; }
			set
			{
				hasPassed = value;
				OnPropertyChanged(nameof(HasPassed));
			}
		}

		public ObservableCollection<TreeItem> ChildItems { get; set; } = new ObservableCollection<TreeItem>();
	}

	public class Parent : TreeItem
	{
		public Parent(string name) : base(name)
		{
		}
		public ObservableCollection<TreeItem> ChildItems { get; set; } = new ObservableCollection<TreeItem>();
	}

	public class Session : TreeItem
	{
		public Session(string name) : base(name)
		{
		}
	}

	public class MainViewModel : ViewModelBase
	{
		public Logger Logger { get; set; }

		public ObservableCollection<Parent> TreeViewItems { get; set; }

		public ExcelFileWriter Writer { get; set; }

		public MainViewModel()
		{
			this.TreeViewItems = new ObservableCollection<Parent>();
			openAddCommand = new RelayCommand(p => OpenAddDialog(), p => CommandCanExecute());
			openEditCommand = new RelayCommand(p => OpenEditDialog(), p => CommandCanExecute());
			openDeleteCommand = new RelayCommand(p => OpenDeleteDialog(), p => CommandCanExecute());
			exportExcelXlsxCommand = new RelayCommand(p => ExportExcelXlsx(), p => CommandCanExecute());
			exportExcelXlsCommand = new RelayCommand(p => ExportExcelXls(), p => CommandCanExecute());
			exportJsonCommand = new RelayCommand(p => ExportJson(), p => CommandCanExecute());
			exportHtmlCommand = new RelayCommand(p => ExportHtml(), p => CommandCanExecute());
			exportCsvCommand = new RelayCommand(p => ExportCsv(), p => CommandCanExecute());
			exportXmlCommand = new RelayCommand(p => ExportXml(), p => CommandCanExecute());
			importCommand = new RelayCommand(p => Import(), p => true);
			openVumCommand = new RelayCommand(p => OpenVum(), p => true);
			printCommand = new RelayCommand(p => Print(), p => CommandCanExecute());
			clearCommand = new RelayCommand(p => Clear(), p => CommandCanExecute());
			saveCommand = new RelayCommand(p => Save(), p => CommandCanExecute());
			openLocationCommand = new RelayCommand(p => OpenLocation(), p => true);
			openLogLocationCommand = new RelayCommand(p => OpenLogLocation(), p => true);
			Logger = new Logger();
		}

		private void OpenLogLocation()
		{
			System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo()
			{
				FileName = Directory.GetCurrentDirectory() + "/LogOutput",
				UseShellExecute = true,
				Verb = "open"
			});

			Logger.LogInfo(new LogModel
			{
				Message = $"Log Location Opened",
				EventType = EventType.Neutral,
				LogType = LogType.Information
			});
		}

		private void ExportJson()
		{
			NotifyIcon trayIcon = new NotifyIcon
			{
				Icon = new System.Drawing.Icon("Assets/ButtonIcons/JSONIcon.ico"),
				Visible = true
			};

			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				Title = "Export as..."
			};

			DialogResult result = saveFileDialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				string filePath = saveFileDialog.InitialDirectory + saveFileDialog.FileName;
				try
				{
					string tempName = $"Output/temp{ DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") }.xlsx";
					if (File.Exists(tempName))
						File.Delete(tempName);

					Writer.SaveFile(tempName);
					WorkBook workbook = WorkBook.Load(tempName);
					foreach (WorkSheet sheet in workbook.WorkSheets)
					{
						try
						{
							sheet.SaveAsJson($"{ filePath }-{ sheet.Name }.json");
						}
						catch (Exception ex)
						{
							MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
							Logger.LogError(new LogModel
							{
								Message = $"An error occured for '{ filePath }'.json",
								ExceptionMessage = ex.Message,
								BaseException = ex.GetBaseException().Message,
								InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
								StackTrace = ex.StackTrace,
								EventType = EventType.Failure,
								LogType = LogType.Error
							});
						}
					}

					trayIcon.ShowBalloonTip(0, "Export JSON", "Json File Exported Successfully!", ToolTipIcon.None);
					Logger.LogInfo(new LogModel
					{
						Message = $"'{ filePath }' saved as JSON successfully",
						EventType = EventType.Success,
						LogType = LogType.Information
					});
				} 
				catch (Exception ex)
				{
					MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
					Logger.LogError(new LogModel
					{
						Message = $"'{ filePath }' wasn't exported as JSON",
						ExceptionMessage = ex.Message,
						BaseException = ex.GetBaseException().Message,
						InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
						StackTrace = ex.StackTrace,
						EventType = EventType.Failure,
						LogType = LogType.Error
					});
				}
			}
		}

		private void ExportCsv()
		{
			NotifyIcon trayIcon = new NotifyIcon
			{
				Icon = new System.Drawing.Icon("Assets/ButtonIcons/CSVIcon.ico"),
				Visible = true
			};

			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				Title = "Export as..."
			};

			DialogResult result = saveFileDialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				string filePath = saveFileDialog.InitialDirectory + saveFileDialog.FileName;

				try
				{
					string tempName = $"Output/temp{ DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") }.xlsx";
					if (File.Exists(tempName))
						File.Delete(tempName);

					Writer.SaveFile(tempName);
					WorkBook workbook = WorkBook.Load(tempName);
					foreach (WorkSheet sheet in workbook.WorkSheets)
					{
						try
						{
							sheet.SaveAsCsv($"{ filePath }-{ sheet.Name }.csv");
						}
						catch (Exception ex)
						{
							MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
							Logger.LogError(new LogModel
							{
								Message = $"An error occured for '{ filePath }'.csv",
								ExceptionMessage = ex.Message,
								BaseException = ex.GetBaseException().Message,
								InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
								StackTrace = ex.StackTrace,
								EventType = EventType.Failure,
								LogType = LogType.Error
							});
						}
					}
					trayIcon.ShowBalloonTip(0, "Export CSV", "CSV File Exported Successfully!", ToolTipIcon.None);
					Logger.LogInfo(new LogModel
					{
						Message = $"'{ filePath }' saved as CSV successfully",
						EventType = EventType.Success,
						LogType = LogType.Information
					});
				}
				catch(Exception ex)
				{
					MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
					Logger.LogError(new LogModel
					{
						Message = $"'{ filePath }' wasn't exported as CSV",
						ExceptionMessage = ex.Message,
						BaseException = ex.GetBaseException().Message,
						InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
						StackTrace = ex.StackTrace,
						EventType = EventType.Failure,
						LogType = LogType.Error
					});
				}
			}
		}

		private void ExportXml()
		{
			NotifyIcon trayIcon = new NotifyIcon
			{
				Icon = new System.Drawing.Icon("Assets/ButtonIcons/XMLIcon.ico"),
				Visible = true
			};

			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				Title = "Export as..."
			};

			DialogResult result = saveFileDialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				string filePath = saveFileDialog.InitialDirectory + saveFileDialog.FileName;

				try
				{
					string tempName = $"Output/temp{ DateTime.Now.ToString("dd.MM.yyyy_HH.mm.ss") }.xlsx";
					if (File.Exists(tempName))
						File.Delete(tempName);

					Writer.SaveFile(tempName);
					WorkBook workbook = WorkBook.Load(tempName);
					foreach (WorkSheet sheet in workbook.WorkSheets)
					{
						try
						{
							sheet.SaveAsCsv($"{ filePath }-{ sheet.Name }.xml");
						}
						catch (Exception ex)
						{
							MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
							Logger.LogError(new LogModel
							{
								Message = $"An error occured for '{ filePath }.xml'",
								ExceptionMessage = ex.Message,
								BaseException = ex.GetBaseException().Message,
								InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
								StackTrace = ex.StackTrace,
								EventType = EventType.Failure,
								LogType = LogType.Error
							});
						}
					}
					trayIcon.ShowBalloonTip(0, "Export XML", "XML File Exported Successfully!", ToolTipIcon.None);
					Logger.LogInfo(new LogModel
					{
						Message = $"'{ filePath }' saved as XML successfully",
						EventType = EventType.Success,
						LogType = LogType.Information
					});
				}
				catch (Exception ex)
				{
					MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
					Logger.LogError(new LogModel
					{
						Message = $"'{ filePath }' wasn't exported as XML",
						ExceptionMessage = ex.Message,
						BaseException = ex.GetBaseException().Message,
						InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
						StackTrace = ex.StackTrace,
						EventType = EventType.Failure,
						LogType = LogType.Error
					});
				}
			}
		}

		private ICommand printCommand;
		public ICommand PrintCommand
		{
			get { return printCommand; }
			set
			{
				printCommand = value;
				OnPropertyChanged(nameof(PrintCommand));
			}
		}

		private ICommand saveCommand;
		public ICommand SaveCommand
		{
			get { return saveCommand; }
			set
			{
				saveCommand = value;
				OnPropertyChanged(nameof(SaveCommand));
			}
		}

		private ICommand openLocationCommand;
		public ICommand OpenLocationCommand
		{
			get { return openLocationCommand; }
			set
			{
				openLocationCommand = value;
				OnPropertyChanged(nameof(OpenLocationCommand));
			}
		}

		private ICommand clearCommand;
		public ICommand ClearCommand
		{
			get { return clearCommand; }
			set
			{
				clearCommand = value;
				OnPropertyChanged(nameof(ClearCommand));
			}
		}

		public List<ExcelColumn> ExcelColumns { get; set; }

		private ICommand exportExcelXlsxCommand;
		public ICommand ExportExcelXlsxCommand
		{
			get { return exportExcelXlsxCommand; }
			set
			{
				exportExcelXlsxCommand = value;
				OnPropertyChanged(nameof(ExportExcelXlsxCommand));
			}
		}

		private ICommand openLogLocationCommand;
		public ICommand OpenLogLocationCommand
		{
			get { return openLogLocationCommand; }
			set
			{
				openLogLocationCommand = value;
				OnPropertyChanged(nameof(OpenLogLocationCommand));
			}
		}

		private ICommand exportExcelXlsCommand;
		public ICommand ExportExcelXlsCommand
		{
			get { return exportExcelXlsCommand; }
			set
			{
				exportExcelXlsCommand = value;
				OnPropertyChanged(nameof(ExportExcelXlsCommand));
			}
		}

		private ICommand exportJsonCommand;
		public ICommand ExportJsonCommand
		{
			get { return exportJsonCommand; }
			set
			{
				exportJsonCommand = value;
				OnPropertyChanged(nameof(ExportJsonCommand));
			}
		}

		private ICommand exportXmlCommand;
		public ICommand ExportXmlCommand
		{
			get { return exportXmlCommand; }
			set
			{
				exportXmlCommand = value;
				OnPropertyChanged(nameof(ExportXmlCommand));
			}
		}

		private ICommand exportCsvCommand;
		public ICommand ExportCsvCommand
		{
			get { return exportCsvCommand; }
			set
			{
				exportCsvCommand = value;
				OnPropertyChanged(nameof(ExportCsvCommand));
			}
		}

		private ICommand exportHtmlCommand;
		public ICommand ExportHtmlCommand
		{
			get { return exportHtmlCommand; }
			set
			{
				exportHtmlCommand = value;
				OnPropertyChanged(nameof(ExportHtmlCommand));
			}
		}

		private string search;
		public string Search
		{
			get { return search; }
			set
			{
				search = value;
				OnPropertyChanged(nameof(Search));
				PerformSearch(search);
			}
		}

		private ICommand openVumCommand;
		public ICommand OpenVumCommand
		{
			get { return openVumCommand; }
			set
			{
				openVumCommand = value;
				OnPropertyChanged(nameof(OpenVumCommand));
			}
		}

		private ICommand importCommand;
		public ICommand ImportCommand
		{
			get { return importCommand; }
			set
			{
				importCommand = value;
				OnPropertyChanged(nameof(ImportCommand));
			}
		}

		private ICommand openAddCommand;
		public ICommand OpenAddCommand
		{
			get { return openAddCommand; }
			set
			{
				openAddCommand = value;
				OnPropertyChanged(nameof(OpenAddCommand));
			}
		}

		private ICommand openEditCommand;
		public ICommand OpenEditCommand
		{
			get { return openEditCommand; }
			set
			{
				openEditCommand = value;
				OnPropertyChanged(nameof(OpenEditCommand));
			}
		}

		private ICommand openDeleteCommand;
		public ICommand OpenDeleteCommand
		{
			get { return openDeleteCommand; }
			set
			{
				openDeleteCommand = value;
				OnPropertyChanged(nameof(OpenDeleteCommand));
			}
		}

		private void OpenAddDialog()
		{
			ApplicationController.GetInstance().OpenWindow(ApplicationWindow.AddDialog, new AddDialogViewModel(this), true);
		}

		private void OpenEditDialog()
		{
			ApplicationController.GetInstance().OpenWindow(ApplicationWindow.EditDialog, new EditDialogViewModel(this), true);
		}

		private void OpenDeleteDialog()
		{
			ApplicationController.GetInstance().OpenWindow(ApplicationWindow.DeleteDialog, new DeleteDialogViewModel(this), true);
		}

		private void Clear()
		{
			Search = string.Empty;
		}

		private void PerformSearch(string search)
		{
			TreeViewItems.Clear();

			List<string> groupNames = new List<string>();
			foreach (var column in ExcelColumns)
			{
				foreach (string groupName in column.Group.Names)
				{
					groupNames.Add(groupName);
				}
			}

			foreach (var group in groupNames.Distinct())
			{
				ObservableCollection<TreeItem> subjects = new ObservableCollection<TreeItem>();

				string currentGroupName = group;
				foreach (ExcelColumn column in ExcelColumns.Where(x => x.Group.Names.Contains(group)))
				{
					string subjectName = column.Subject.Name;
					Parent folder = new Parent(subjectName);

					if (subjectName.ToLower().Contains(search))
					{
						Parent lecturers = new Parent($"Lecturers (Count: { column.Subject.LecturerNames.Count })");
						foreach (string lecturerName in column.Subject.LecturerNames)
						{
							lecturers.ChildItems.Add(new Parent(lecturerName));
						}

						Parent groups = new Parent($"Groups (Count: { column.Group.Names.Count })");
						foreach (string groupName in column.Group.Names)
						{
							groups.ChildItems.Add(new Parent(groupName));
						}

						List<SubjectCell> orderedSubjectCells = column.Subject.SubjectCells.OrderBy(x => x.Date).ToList();
						Parent lectures = new Parent($"Lectures (Count: { orderedSubjectCells.Where(x => !string.IsNullOrEmpty(x.ValueOfCell)).Count() })");
						foreach (SubjectCell lectureDate in orderedSubjectCells)
						{
							if (!string.IsNullOrEmpty(lectureDate.ValueOfCell))
							{
								string startingHour = lectureDate.StartingHour.ToString("HH:mm");
								string endingHour = lectureDate.EndingHour.ToString("HH:mm");

								string lectureString = $"{ startingHour } - { endingHour } ({ lectureDate.Date.ToString("dd MMMM yyyy") }) { lectureDate.ValueOfCell } ";

								if (lectureDate.Date < DateTime.Now)
								{
									Parent parent = new Parent(lectureString += "(Passed)");
									parent.HasPassed = true;
									lectures.ChildItems.Add(parent);
								}
								else
								{
									Parent parent = new Parent(lectureString);
									parent.HasPassed = false;
									lectures.ChildItems.Add(parent);
								}
							}
						}
						Parent temp = new Parent(subjectName);
						temp.ChildItems.Add(lecturers);
						temp.ChildItems.Add(groups);
						temp.ChildItems.Add(lectures);

						subjects.Add(temp);
					}
				}

				Parent root = new Parent(currentGroupName) { ChildItems = subjects };

				if (subjects.Count == 0)
				{
					root.Name += " (No matching subjects found)";
					root.HasPassed = true;
				}

				TreeViewItems.Add(root);
			}
		}

		private void Save()
		{
			string fileName = $"SaveFiles/Schedule_{DateTime.Now.ToString("dd.MM.yyyy_HH.mm")}.xlsx";

			try
			{
				Writer.SaveFile(fileName);
				Logger.LogInfo(new LogModel
				{
					Message = $"'{ fileName }' saved successfully",
					EventType = EventType.Success,
					LogType = LogType.Information
				});
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				Logger.LogError(new LogModel
				{
					Message = $"'{ fileName }' wasn't saved",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		private void OpenLocation()
		{
			System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo()
			{
				FileName = Directory.GetCurrentDirectory() + "/SaveFiles",
				UseShellExecute = true,
				Verb = "open"
			});

			Logger.LogInfo(new LogModel
			{
				Message = $"Save Location Opened",
				EventType = EventType.Neutral,
				LogType = LogType.Information
			});
		}

		public bool CommandCanExecute()
		{
			return TreeViewItems?.Count > 0;
		}

		private void Import()
		{
			OpenFileDialog openFileDialog = new OpenFileDialog
			{
				Title = "Import file",
				Filter = "Excel Files|*.xls;*.xlsx;"
			};
			DialogResult result = openFileDialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				string filePath = openFileDialog.InitialDirectory + openFileDialog.FileName;

				try
				{
					ExcelParser parser = new ExcelParser();
					List<ExcelColumn> columns = parser.Parse(filePath);
					ExcelColumns = columns;
					Writer = new ExcelFileWriter(new FileStream(columns.ElementAt(0).FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite));
					Load(columns);

					Logger.LogInfo(new LogModel
					{
						Message = $"'{ filePath }' Imported successfully",
						EventType = EventType.Success,
						LogType = LogType.Information
					});
				}
				catch (Exception ex)
				{
					MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
					Logger.LogError(new LogModel
					{
						Message = $"Couldn't import '{ filePath }'",
						ExceptionMessage = ex.Message,
						BaseException = ex.GetBaseException().Message,
						InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
						StackTrace = ex.StackTrace,
						EventType = EventType.Failure,
						LogType = LogType.Error
					});
				}
			}
			openFileDialog.Dispose();
		}

		public void Refresh()
		{
			Load(ExcelColumns);
		}

		public void Load(List<ExcelColumn> columns)
		{
			TreeViewItems.Clear();
			List<string> groupNames = new List<string>();
			foreach (var column in columns)
			{
				foreach (string groupName in column.Group.Names)
				{
					groupNames.Add(groupName);
				}
			}

			foreach (var group in groupNames.Distinct())
			{
				ObservableCollection<TreeItem> subjects = new ObservableCollection<TreeItem>();

				foreach (ExcelColumn column in columns.Where(x => x.Group.Names.Contains(group)))
				{
					string subjectName = column.Subject.Name;
					Parent folder = new Parent(subjectName);

					Parent lecturers = new Parent($"Lecturers (Count: { column.Subject.LecturerNames.Count })");
					foreach (string lecturerName in column.Subject.LecturerNames)
					{
						lecturers.ChildItems.Add(new Parent(lecturerName));
					}

					Parent groups = new Parent($"Groups (Count: { column.Group.Names.Count })");
					foreach (string groupName in column.Group.Names)
					{
						groups.ChildItems.Add(new Parent(groupName));
					}

					List<SubjectCell> orderedSubjectCells = column.Subject.SubjectCells.OrderByDescending(x => x.Date).ToList();
					Parent lectures = new Parent($"Lectures (Count: { orderedSubjectCells.Where(x => !string.IsNullOrEmpty(x.ValueOfCell)).Count() })");
					foreach (SubjectCell lectureDate in orderedSubjectCells)
					{
						if (!string.IsNullOrEmpty(lectureDate.ValueOfCell))
						{
							string startingHour = lectureDate.StartingHour.ToString("HH:mm");
							string endingHour = lectureDate.EndingHour.ToString("HH:mm");

							string lectureString = $"{ startingHour } - { endingHour } ({ lectureDate.Date.ToString("dd MMMM yyyy") }) { lectureDate.ValueOfCell } ";

							if (lectureDate.Date < DateTime.Now)
							{
								Parent parent = new Parent(lectureString += "(Passed)");
								parent.HasPassed = true;
								lectures.ChildItems.Add(parent);
							}
							else
							{
								Parent parent = new Parent(lectureString);
								parent.HasPassed = false;
								lectures.ChildItems.Add(parent);
							}
						}
					}
					Parent temp = new Parent(subjectName);
					temp.ChildItems.Add(lecturers);
					temp.ChildItems.Add(groups);
					temp.ChildItems.Add(lectures);

					subjects.Add(temp);
				}

				TreeViewItems.Add(new Parent(group) { ChildItems = subjects });
			}
		}

		private void OpenVum()
		{
			var processStartInfo = new ProcessStartInfo("https://vum.bg/")
			{
				UseShellExecute = true,
				Verb = "open"
			};
			Process.Start(processStartInfo);

			Logger.LogInfo(new LogModel
			{
				Message = $"VUM Website Opened",
				EventType = EventType.Neutral,
				LogType = LogType.Information
			});
		}

		private void ExportExcelXlsx()
		{
			NotifyIcon trayIcon = new NotifyIcon
			{
				Icon = new System.Drawing.Icon("Assets/ButtonIcons/ExcelIcon.ico"),
				Visible = true
			};

			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				Title = "Export as..."
			};

			DialogResult result = saveFileDialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				string filePath = saveFileDialog.InitialDirectory + saveFileDialog.FileName;

				try
				{
					Writer.SaveFile($"{ filePath }.xlsx");
					trayIcon.ShowBalloonTip(0, "Export Excel", "Excel File Exported Successfully!", ToolTipIcon.None);

					Logger.LogInfo(new LogModel
					{
						Message = $"'{ filePath }' saved as XLSX successfully",
						EventType = EventType.Neutral,
						LogType = LogType.Information
					});
				}
				catch (Exception ex)
				{
					MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
					Logger.LogError(new LogModel
					{
						Message = $"Couldn't save '{ filePath }' as xlsx",
						ExceptionMessage = ex.Message,
						BaseException = ex.GetBaseException().Message,
						InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
						StackTrace = ex.StackTrace,
						EventType = EventType.Failure,
						LogType = LogType.Error
					});
				}
			}
		}

		private void ExportExcelXls()
		{
			NotifyIcon trayIcon = new NotifyIcon
			{
				Icon = new System.Drawing.Icon("Assets/ButtonIcons/ExcelIconXls.ico"),
				Visible = true
			};

			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				Title = "Export as..."
			};

			DialogResult result = saveFileDialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				string filePath = saveFileDialog.InitialDirectory + saveFileDialog.FileName;

				try
				{
					Writer.SaveFile($"{ filePath }.xls");
					trayIcon.ShowBalloonTip(0, "Export Excel", "Excel File Exported Successfully!", ToolTipIcon.None);

					Logger.LogInfo(new LogModel
					{
						Message = $"'{ filePath }' saved as XLS successfully",
						EventType = EventType.Neutral,
						LogType = LogType.Information
					});
				}
				catch (Exception ex)
				{
					MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
					Logger.LogError(new LogModel
					{
						Message = $"Couldn't save '{ filePath }' as xls",
						ExceptionMessage = ex.Message,
						BaseException = ex.GetBaseException().Message,
						InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
						StackTrace = ex.StackTrace,
						EventType = EventType.Failure,
						LogType = LogType.Error
					});
				}
			}
		}

		private void ExportHtml()
		{
			NotifyIcon trayIcon = new NotifyIcon
			{
				Icon = new System.Drawing.Icon("Assets/ButtonIcons/HTMLIcon.ico"),
				Visible = true
			};

			SaveFileDialog saveFileDialog = new SaveFileDialog
			{
				Title = "Export as..."
			};

			DialogResult result = saveFileDialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				string filePath = saveFileDialog.InitialDirectory + saveFileDialog.FileName;
				IHtmlGenerator generator = new HtmlCompactGenerator();
				generator.Generate(ExcelColumns, filePath);
				trayIcon.ShowBalloonTip(0, "Export HTML", "HTML Exported Successfully!", ToolTipIcon.None);
			}
		}

		private void Print()
		{
			// TO DO
			PrintDialog printDialog = new PrintDialog
			{
				AllowSomePages = true,
				ShowHelp = true
			};

			PrintDocument printDocument = new PrintDocument();

			//printDialog.Document = printDocument;

			DialogResult result = printDialog.ShowDialog();

			// If the result is OK then print the document.
			if (result == System.Windows.Forms.DialogResult.OK)
			{
				//printDocument.Print();
			}

			printDocument.Dispose();
			printDialog.Dispose();
		}
	}
}
