﻿using LectureScheduleManagementSystem.Extensions;
using LectureScheduleManagementSystem.FileFormats;
using LectureScheduleManagementSystem.Helpers;
using LectureScheduleManagementSystem.Logging;
using LectureScheduleManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;

namespace LectureScheduleManagementSystem.ViewModels
{
	public class AddDialogViewModel : ViewModelBase
	{
		public MainViewModel MainViewModel { get; set; }
		private ObservableCollection<Parent> treeViewValues = new ObservableCollection<Parent>();

		public ObservableCollection<Parent> TreeViewValues
		{
			get { return treeViewValues; }
			set
			{
				treeViewValues = value;
				OnPropertyChanged(nameof(TreeViewValues));
			}
		}

		public AddDialogViewModel(MainViewModel mainViewModel)
		{
			Title = "Add";
			MainViewModel = mainViewModel;
			BindTreeViewValues();
			BindGroups();
			BindSubjectCells();
			addSubjectCellCommand = new RelayCommand(p => AddSubjectCell(), p => true);
			removeSubjectCellCommand = new RelayCommand(p => RemoveSubjectCell(), p => true);
			addCommand = new RelayCommand(p => Add(), p => CommandCanExecute());
			applyChangeCommand = new RelayCommand(p => ApplyChange(), p => CommandCanExecute());
		}

		public void BindSubjectCells()
		{
			SubjectCells.Add(new SubjectCell
			{
				Date = DateTime.Today,
				ValueOfCell = "ex. Software Metrics (6)",
				StartingHour = DateTime.Now,
				EndingHour = DateTime.Now
			});
		}

		public void BindGroups()
		{
			try
			{
				ObservableCollection<string> groups = new ObservableCollection<string>();
				foreach (var item in MainViewModel.TreeViewItems)
				{
					groups.Add(item.Name);
				}
				Groups = groups;
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"BindGroups in AddDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		public void BindSubjects()
		{
			try
			{
				ObservableCollection<string> subjects = new ObservableCollection<string>();
				foreach (var subject in MainViewModel.TreeViewItems.Where(x => x.Name == SelectedGroup))
				{
					foreach (var item in subject.ChildItems)
					{
						subjects.Add(item.Name);
					}
				}
				Subjects = subjects;
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"BindSubjects in AddDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		public void BindTreeViewValues()
		{
			foreach (var item in MainViewModel.TreeViewItems)
			{
				foreach (var subject in item.ChildItems)
				{
					treeViewValues.Add(new Parent(subject.Name));
				}
			}

			treeViewValues = treeViewValues.DistinctBy(x => x.Name);
		}

		public void Add()
		{
			var subject = treeViewValues.Where(p => p.Name == SelectedSubject).FirstOrDefault();
			var childItems = subject.ChildItems;
			childItems.Add(new Parent(SubjectName)
			{
				Name = SubjectName
			});
			childItems.LastOrDefault().ChildItems.Add(new Parent(LecturerName)
			{
				Name = LecturerName
			});
		}

		public void AddSubjectCell()
		{
			var newSubjectCell = new SubjectCell
			{
				Date = DateTime.Today,
				ValueOfCell = "ex. Software Metrics (6)",
				StartingHour = DateTime.Now,
				EndingHour = DateTime.Now
			};
			SubjectCells.Add(newSubjectCell);
		}

		public void RemoveSubjectCell()
		{
			SubjectCells.Remove(SelectedSubjectCell);
		}

		public void ApplyChange()
		{
			if (SubjectCells.Where(p => p.ValueOfCell == "ex. Software Metrics (6)").Any())
			{
				MessageBoxHelpers.ShowWarning(Constants.SubjectMessage, Constants.SubjectCaption);
			}
			else
			{
				try
				{
					ExcelFileWriter writer = MainViewModel.Writer;
					using (FileStream file = new FileStream(MainViewModel.ExcelColumns.FirstOrDefault().FilePath, FileMode.Open, FileAccess.Read))
					{
						ExcelFileReader excelFileReader = new ExcelFileReader(file);

						for (int column = 0; column <= excelFileReader.GetColumnCount("All"); column++)
						{
							string currentSubjectName = excelFileReader.ReadCell(new CellCoordinates(0, column), "All");
							string currentLecturerName = excelFileReader.ReadCell(new CellCoordinates(1, column), "All");
							string currentGroupName = excelFileReader.ReadCell(new CellCoordinates(3, column), "All");

							//Hijack the empty column, wherever it is
							if (string.IsNullOrEmpty(currentSubjectName) &&
							   string.IsNullOrEmpty(currentLecturerName) &&
							   string.IsNullOrEmpty(currentGroupName))
							{
								int emptyColumnIndex = column;
								writer.WriteCell(new Cell(new CellCoordinates(0, emptyColumnIndex), SubjectName), "All");
								writer.WriteCell(new Cell(new CellCoordinates(1, emptyColumnIndex), LecturerName), "All");
								writer.WriteCell(new Cell(new CellCoordinates(3, emptyColumnIndex), GroupName), "All");

								foreach (var item in subjectCells)
								{
									foreach (var excelColumn in MainViewModel.ExcelColumns)
									{
										//Just take the row
										var pair = excelColumn.Subject.DateCells.Where(x => x.Key.Date.Year == item.Date.Year && x.Key.Date.Month == item.Date.Month && x.Key.Date.Day == item.Date.Day).FirstOrDefault();
										int row = pair.Value;
										writer.WriteCell(new Cell(row, emptyColumnIndex, $"{ item.StartingHour.ToString("HH.mm") } - { item.EndingHour.ToString("HH.mm") } { item.ValueOfCell }"), "All");

										//SubjectCell subjectCell = new SubjectCell();
										//	subjectCell.CellCoordinates = new CellCoordinates(row, column);
										//	subjectCell.Date = item.Date;
										//	subjectCell.StartingHour = item.StartingHour;
										//	subjectCell.EndingHour = item.EndingHour;
										//	subjectCell.ValueOfCell = item.ValueOfCell;
									}
								}
							}
						}
					}

					ExcelColumn excelColumnToAdd = new ExcelColumn();
					excelColumnToAdd.Subject = new Subject { Name = SubjectName, SubjectCells = SubjectCells.ToList() };
					excelColumnToAdd.Lecturer = new Lecturer { Name = LecturerName };

					if(GroupName.Contains(','))
					{
						excelColumnToAdd.Group = new Group { Names = GroupName.Split(',').ToList() };
					}
					else
					{
						List<string> temp = new List<string> { GroupName };
						excelColumnToAdd.Group = new Group { Names = temp };
					}

					MessageBoxHelpers.ShowInformation(Constants.AddMessage, Constants.AddCaption);
					MainViewModel.Logger.LogInfo(new LogModel
					{
						Message = $"Adding success!",
						EventType = EventType.Success,
						LogType = LogType.Information
					});
				}
				catch (Exception ex)
				{
					MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
					MainViewModel.Logger.LogError(new LogModel
					{
						Message = $"Applying Changes in AddDialog Failed",
						ExceptionMessage = ex.Message,
						BaseException = ex.GetBaseException().Message,
						InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
						StackTrace = ex.StackTrace,
						EventType = EventType.Failure,
						LogType = LogType.Error
					});
				}
			}
		}

		public bool CommandCanExecute()
		{
			return SubjectCells?.Count > 0 && !string.IsNullOrEmpty(SubjectName) && !string.IsNullOrEmpty(GroupName) && !string.IsNullOrEmpty(LecturerName);
		}

		private ObservableCollection<SubjectCell> subjectCells = new ObservableCollection<SubjectCell>();
		public ObservableCollection<SubjectCell> SubjectCells
		{
			get { return subjectCells; }
			set
			{
				subjectCells = value;
				OnPropertyChanged(nameof(SubjectCells));
			}
		}

		private SubjectCell selectedSubjectCell;
		public SubjectCell SelectedSubjectCell
		{
			get { return selectedSubjectCell; }
			set
			{
				selectedSubjectCell = value;
				OnPropertyChanged(nameof(SelectedSubjectCell));
			}
		}

		private ObservableCollection<string> subjects;
		public ObservableCollection<string> Subjects
		{
			get { return subjects; }
			set
			{
				subjects = value;
				OnPropertyChanged(nameof(Subjects));
			}
		}

		private ObservableCollection<string> groups;
		public ObservableCollection<string> Groups
		{
			get { return groups; }
			set
			{
				groups = value;
				OnPropertyChanged(nameof(Groups));
			}
		}

		private string selectedGroup;
		public string SelectedGroup
		{
			get { return selectedGroup; }
			set
			{
				selectedGroup = value;
				OnPropertyChanged(nameof(SelectedGroup));
				BindSubjects();
			}
		}

		private string selectedSubject;
		public string SelectedSubject
		{
			get { return selectedSubject; }
			set
			{
				selectedSubject = value;
				OnPropertyChanged(nameof(SelectedSubject));
			}
		}

		private string subjectName;
		public string SubjectName
		{
			get
			{
				return subjectName;
			}
			set
			{
				subjectName = value;
				OnPropertyChanged(nameof(SubjectName));
			}
		}

		private string groupName;
		public string GroupName
		{
			get
			{
				return groupName;
			}
			set
			{
				groupName = value;
				OnPropertyChanged(nameof(GroupName));
			}
		}

		private string lecturerName;
		public string LecturerName
		{
			get
			{
				return lecturerName;
			}
			set
			{
				lecturerName = value;
				OnPropertyChanged(nameof(LecturerName));
			}
		}

		private ICommand addCommand;
		public ICommand AddCommand
		{
			get { return addCommand; }
			set
			{
				addCommand = value;
				OnPropertyChanged(nameof(AddCommand));
			}
		}

		private ICommand applyChangeCommand;
		public ICommand ApplyChangeCommand
		{
			get { return applyChangeCommand; }
			set
			{
				applyChangeCommand = value;
				OnPropertyChanged(nameof(ApplyChangeCommand));
			}
		}

		private ICommand addSubjectCellCommand;
		public ICommand AddSubjectCellCommand
		{
			get { return addSubjectCellCommand; }
			set
			{
				addSubjectCellCommand = value;
				OnPropertyChanged(nameof(AddSubjectCellCommand));
			}
		}

		private ICommand removeSubjectCellCommand;
		public ICommand RemoveSubjectCellCommand
		{
			get { return removeSubjectCellCommand; }
			set
			{
				removeSubjectCellCommand = value;
				OnPropertyChanged(nameof(RemoveSubjectCellCommand));
			}
		}
	}
}
