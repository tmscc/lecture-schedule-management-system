﻿using LectureScheduleManagementSystem.Extensions;
using LectureScheduleManagementSystem.FileFormats;
using LectureScheduleManagementSystem.Helpers;
using LectureScheduleManagementSystem.Logging;
using LectureScheduleManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace LectureScheduleManagementSystem.ViewModels
{
	public class EditDialogViewModel : ViewModelBase
	{
		public MainViewModel MainViewModel { get; set; }
		private ObservableCollection<Parent> treeViewValues = new ObservableCollection<Parent>();

		private ObservableCollection<SubjectCell> subjectCells = new ObservableCollection<SubjectCell>();
		public ObservableCollection<SubjectCell> SubjectCells
		{
			get { return subjectCells; }
			set
			{
				subjectCells = value;
				OnPropertyChanged(nameof(SubjectCells));
			}
		}

		private ICommand addSubjectCellCommand;
		public ICommand AddSubjectCellCommand
		{
			get { return addSubjectCellCommand; }
			set
			{
				addSubjectCellCommand = value;
				OnPropertyChanged(nameof(AddSubjectCellCommand));
			}
		}

		private ICommand removeSubjectCellCommand;
		public ICommand RemoveSubjectCellCommand
		{
			get { return removeSubjectCellCommand; }
			set
			{
				removeSubjectCellCommand = value;
				OnPropertyChanged(nameof(RemoveSubjectCellCommand));
			}
		}

		private SubjectCell selectedSubjectCell;
		public SubjectCell SelectedSubjectCell
		{
			get { return selectedSubjectCell; }
			set
			{
				selectedSubjectCell = value;
				OnPropertyChanged(nameof(SelectedSubjectCell));
			}
		}

		public EditDialogViewModel(MainViewModel mainViewModel)
		{
			Title = "Edit";
			MainViewModel = mainViewModel;
			BindGroups();
			BindSubjectCells();
			BindTreeViewValues();
			editCommand = new RelayCommand(p => Edit(), p => CommandCanExecute());
			addSubjectCellCommand = new RelayCommand(p => AddSubjectCell(), p => true);
			removeSubjectCellCommand = new RelayCommand(p => RemoveSubjectCell(), p => true);
		}

		public void RemoveSubjectCell()
		{
			SubjectCells.Remove(SelectedSubjectCell);
		}

		public void AddSubjectCell()
		{
			var newSubjectCell = new SubjectCell
			{
				Date = DateTime.Today,
				ValueOfCell = string.Empty,
				StartingHour = DateTime.Now,
				EndingHour = DateTime.Now
			};
			SubjectCells.Add(newSubjectCell);
		}

		public void BindGroups()
		{
			try
			{
				ObservableCollection<string> groups = new ObservableCollection<string>();
				foreach (var item in MainViewModel.TreeViewItems)
				{
					groups.Add(item.Name);
				}
				Groups = groups;
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"BindGroups in EditDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		public void BindSubjects()
		{
			try
			{
				ObservableCollection<string> subjects = new ObservableCollection<string>();
				foreach (var subject in MainViewModel.TreeViewItems.Where(x => x.Name == SelectedGroup))
				{
					foreach (var item in subject.ChildItems)
					{
						subjects.Add(item.Name);
					}
				}
				Subjects = subjects;
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"BindSubjects in EditDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		public void BindTreeViewValues()
		{
			foreach (var item in MainViewModel.TreeViewItems)
			{
				foreach (var subject in item.ChildItems)
				{
					treeViewValues.Add(new Parent(subject.Name));
				}
			}

			treeViewValues = treeViewValues.DistinctBy(x => x.Name);
		}

		public static bool DoesNotOverlap(IEnumerable<SubjectCell> meetings)
		{
			DateTime endPrior = DateTime.MinValue;
			foreach (SubjectCell meeting in meetings.OrderBy(x => x.Date))
			{
				if (meeting.StartingHour > meeting.EndingHour)
					return false;
				if (meeting.StartingHour < endPrior)
					return false;
				endPrior = meeting.EndingHour;
			}
			return true;
		}

		public void Edit()
		{
			try
			{
				ExcelFileWriter writer = MainViewModel.Writer;

				ExcelColumn column = MainViewModel.ExcelColumns.Where(x => SelectedGroup.Contains(x.Group.Names) && SelectedSubject == x.Subject.Name).FirstOrDefault();
				int columnIndex = column.Group.CellCoordinates.Column;

				//Clean the column
				foreach (var col in column.Subject.SubjectCells)
				{
					int row = 0;
					foreach (var excelColumn in MainViewModel.ExcelColumns)
					{
						SubjectCell cell = excelColumn.Subject.SubjectCells.Where(x => x.Date.Year == col.Date.Year && x.Date.Month == col.Date.Month && x.Date.Day == col.Date.Day).FirstOrDefault();
						var pair = excelColumn.Subject.DateCells.Where(x => x.Key.Day == col.Date.Day && x.Key.Month == col.Date.Month && x.Key.Year == col.Date.Year).FirstOrDefault();
						row = pair.Value;
					}
					writer.WriteCell(new Cell(new CellCoordinates(row, columnIndex), string.Empty), "All");
				}

				writer.WriteCell(new Cell(new CellCoordinates(0, columnIndex), SubjectName), "All");
				writer.WriteCell(new Cell(new CellCoordinates(1, columnIndex), LecturerName), "All");
				writer.WriteCell(new Cell(new CellCoordinates(3, columnIndex), GroupName), "All");

				List<string> strings = new List<string>();

				//Update it
				foreach (var item in subjectCells)
				{
					foreach (var excelColumn in MainViewModel.ExcelColumns)
					{
						SubjectCell cell = excelColumn.Subject.SubjectCells.Where(x => x.Date.Year == item.Date.Year && x.Date.Month == item.Date.Month && x.Date.Day == item.Date.Day).FirstOrDefault();
						var pair = excelColumn.Subject.DateCells.Where(x => x.Key.Day == item.Date.Day && x.Key.Month == item.Date.Month && x.Key.Year == item.Date.Year).FirstOrDefault();
						int row = pair.Value;
						item.MinutesPerDay = item.EndingHour.Subtract(item.StartingHour).TotalMinutes;

						List<ExcelColumn> validationColumns = MainViewModel.ExcelColumns.Where(x => x.Lecturer.Name == LecturerName.Trim() && !SelectedGroup.Contains(x.Group.Names)).ToList();

						foreach (var validationColumn in validationColumns)
						{
							double minutesPerDay = 0;

							foreach (var validationCell in validationColumn.Subject.SubjectCells)
							{
								if (validationCell.Date.Equals(item.Date))
								{
									validationCell.MinutesPerDay = validationCell.EndingHour.Subtract(validationCell.StartingHour).TotalMinutes;
									minutesPerDay += item.MinutesPerDay;
									minutesPerDay += validationCell.MinutesPerDay;
									if (minutesPerDay > 360) //6 hours
									{
										strings.Add($"'{ LecturerName }' exceeds the maximum hours (6, 360 minutes) per day at '{ validationCell.Date.ToString("dd MMMM yyyy") }' for subjects\n" +
											$"'{ validationColumn.Subject.Name }' { StringHelpers.ConvertStringArrayToString(validationColumn.Group.Names) } and '{ SelectedSubject }' { SelectedGroup }\n\n");
										minutesPerDay = 0;
									}

									if(item.StartingHour < validationCell.EndingHour && validationCell.StartingHour < item.EndingHour)
									{
										strings.Add($"Lecture hours overlap for '{ LecturerName }' at '{ validationCell.Date.ToString("dd MMMM yyyy") }' for subjects\n" +
											$"'{ validationColumn.Subject.Name }' { StringHelpers.ConvertStringArrayToString(validationColumn.Group.Names) } and '{ SelectedSubject }' { SelectedGroup }\n\n");
									}
								}
							}
						}

						string cellValue = string.Empty;
						if (item.StartingHour != DateTime.MinValue)
						{
							cellValue += $"{ item.StartingHour.ToString("HH.mm") } - ";
						}

						if (item.EndingHour != DateTime.MinValue)
						{
							cellValue += $"{ item.EndingHour.ToString("HH.mm") } ";
						}

						Regex regex = new Regex("([A-Z]|[a-z]|[&])+");
						string result = string.Empty;
						MatchCollection contactHoursMatches = regex.Matches(item.ValueOfCell);

						foreach (var match in contactHoursMatches)
						{
							result += match.ToString() + " ";
						}

						cellValue += result;

						if (item.ContactHours != 0)
						{
							cellValue += $"({ item.ContactHours.ToString() })";
						}

						writer.WriteCell(new Cell(row, columnIndex, cellValue), "All");
						//cell = new SubjectCell { ValueOfCell = cellValue, Date = item.Date, StartingHour = item.StartingHour, EndingHour = item.EndingHour, ContactHours = item.ContactHours };
					}
				}

				column.Lecturer.Name = LecturerName.Trim();
				column.Subject.Name = SubjectName.Trim();
				if (GroupName.Contains(','))
				{
					column.Group.Names = GroupName.Trim().Split(',').ToList();
				}
				else
				{
					List<string> temp = new List<string> { GroupName.Trim() };
					column.Group.Names = temp;
				}

				if (strings.Count > 0)
				{
					strings = strings.Distinct().ToList();

					StringBuilder stringBuilder = new StringBuilder();
					foreach (var str in strings)
					{
						stringBuilder.Append(str);
					}
					MessageBoxHelpers.ShowExclamation(stringBuilder.ToString(), Constants.WarningCaption);
				}

				MessageBoxHelpers.ShowInformation(Constants.EditMessage, Constants.EditCaption);

				MainViewModel.Logger.LogInfo(new LogModel
				{
					Message = $"Edit success!",
					EventType = EventType.Success,
					LogType = LogType.Information
				});
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"Edit in EditDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		public void BindSubjectCells()
		{
			if (!string.IsNullOrEmpty(SelectedSubject))
			{
				if (SubjectCells.Count > 0)
				{
					SubjectCells.Clear();
				}

				List<ExcelColumn> columns = MainViewModel.ExcelColumns.Where(x => x.Subject.Name == SelectedSubject && SelectedGroup.Contains(x.Group.Names)).ToList();
				foreach (var column in columns)
				{
					column.Subject.SubjectCells.OrderBy(x => x.Date);
					foreach (var subjectCell in column.Subject.SubjectCells)
					{
						if (!string.IsNullOrEmpty(subjectCell.ValueOfCell))
						{
							Regex regex = new Regex("([A-Z]|[a-z]|[&])+");
							string result = string.Empty;
							MatchCollection contactHoursMatches = regex.Matches(subjectCell.ValueOfCell);

							foreach (var match in contactHoursMatches)
							{
								result += match.ToString() + " ";
							}

							SubjectCells.Add(new SubjectCell
							{
								Date = subjectCell.Date,
								ValueOfCell = $"{ result }({ subjectCell.ContactHours })",
								StartingHour = subjectCell.StartingHour,
								EndingHour = subjectCell.EndingHour,
								CellCoordinates = subjectCell.CellCoordinates,
								ContactHours = subjectCell.ContactHours
							});
						}
					}
				}
			}
			else
			{
				SubjectCells.Add(new SubjectCell
				{
					Date = DateTime.Today,
					ValueOfCell = string.Empty,
					StartingHour = DateTime.Now,
					EndingHour = DateTime.Now
				});
			}
		}

		public bool CommandCanExecute()
		{
			return !string.IsNullOrEmpty(SelectedSubject) && !string.IsNullOrEmpty(SubjectName) && !string.IsNullOrEmpty(GroupName) && !string.IsNullOrEmpty(LecturerName);
		}
		public ObservableCollection<Parent> TreeViewValues
		{
			get { return treeViewValues; }
			set
			{
				treeViewValues = value;
				OnPropertyChanged(nameof(TreeViewValues));
			}
		}

		private ICommand editCommand;
		public ICommand EditCommand
		{
			get { return editCommand; }
			set
			{
				editCommand = value;
				OnPropertyChanged(nameof(EditCommand));
			}
		}

		private ObservableCollection<string> subjects;
		public ObservableCollection<string> Subjects
		{
			get { return subjects; }
			set
			{
				subjects = value;
				OnPropertyChanged(nameof(Subjects));
			}
		}

		private ObservableCollection<string> groups;
		public ObservableCollection<string> Groups
		{
			get { return groups; }
			set
			{
				groups = value;
				OnPropertyChanged(nameof(Groups));
			}
		}

		private string selectedGroup;
		public string SelectedGroup
		{
			get { return selectedGroup; }
			set
			{
				selectedGroup = value;
				OnPropertyChanged(nameof(SelectedGroup));
				BindSubjects();
			}
		}

		private string selectedSubject;
		public string SelectedSubject
		{
			get { return selectedSubject; }
			set
			{
				selectedSubject = value;
				OnPropertyChanged(nameof(SelectedSubject));
				BindTextBoxes();
				BindSubjectCells();
			}
		}

		private void BindTextBoxes()
		{
			try
			{
				ExcelColumn column = MainViewModel.ExcelColumns.Where(x => x.Subject.Name == SelectedSubject && SelectedGroup.Contains(x.Group.Names)).FirstOrDefault();
				if (column != null)
				{
					SubjectName = column.Subject.Name;
					StringBuilder stringBuilder = new StringBuilder();
					foreach (var item in column.Group.Names)
					{
						stringBuilder.Append(item + ", ");
					}
					stringBuilder.Remove(stringBuilder.Length - 2, 1);
					GroupName = stringBuilder.ToString();

					stringBuilder.Clear();
					foreach (var item in column.Subject.LecturerNames)
					{
						stringBuilder.Append(item + ", ");
					}
					stringBuilder.Remove(stringBuilder.Length - 2, 1);
					LecturerName = stringBuilder.ToString();
				}
			}
			catch (Exception ex)
			{
				MessageBoxHelpers.ShowError(ex.Message + "\n\n" + ex.GetBaseException().Message + "\n\n" + ex.StackTrace);
				MainViewModel.Logger.LogError(new LogModel
				{
					Message = $"BindTextBoxes in EditDialog Failed",
					ExceptionMessage = ex.Message,
					BaseException = ex.GetBaseException().Message,
					InnerExceptionMessage = ex.InnerException?.Message ?? ex.Message,
					StackTrace = ex.StackTrace,
					EventType = EventType.Failure,
					LogType = LogType.Error
				});
			}
		}

		private string subjectName;
		public string SubjectName
		{
			get
			{
				return subjectName;
			}
			set
			{
				subjectName = value;
				OnPropertyChanged(nameof(SubjectName));
			}
		}

		private string groupName;
		public string GroupName
		{
			get
			{
				return groupName;
			}
			set
			{
				groupName = value;
				OnPropertyChanged(nameof(GroupName));
			}
		}

		private string lecturerName;
		public string LecturerName
		{
			get
			{
				return lecturerName;
			}
			set
			{
				lecturerName = value;
				OnPropertyChanged(nameof(LecturerName));
			}
		}
	}
}