﻿using System;
using System.Reflection;

namespace LectureScheduleManagementSystem.Logging
{
    /// <summary>
    /// 
    /// </summary>
    public class LogModel
    {
        public string Application => Assembly.GetEntryAssembly().GetName().Name;
        public string Message { get; set; }
        public LogType LogType { get; set; }
        public string ExceptionMessage { get; set; }
        public string StackTrace { get; set; }
        public string InnerExceptionMessage { get; set; }
        public string BaseException { get; set; }
        public EventType EventType { get; set; }
        public string CreatedDate => DateTime.Now.ToString("hh:mm - dd MMMM yyyy");
    }
}
