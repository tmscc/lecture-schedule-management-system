﻿using Serilog;
using Serilog.Formatting;
using Serilog.Formatting.Compact;
using System;

namespace LectureScheduleManagementSystem.Logging
{
	/// <summary>
	/// 
	/// </summary>
	public enum FormatterType
	{
		RenderedCompactJsonFormatter,
		CompactJsonFormatter
	}

	/// <summary>
	/// </summary>
	/// <example>
	/// <code>
	/// </code>
	/// </example>
	public class Logger : ILogger
	{
		private readonly ISerializationManager serializationManager;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="formatterType"></param>
		public Logger()
		{
			serializationManager = new SerializationManager();

			Log.Logger = new LoggerConfiguration()
					   .WriteTo.Console()
					   .MinimumLevel.Information()
					   .WriteTo.File(new RenderedCompactJsonFormatter(), $"LogOutput/log_{ DateTime.Now.ToString("dd.MM.yyyy_hh.mm") }.json")
					   .CreateLogger();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		public void LogError<T>(T obj)
		{
			var data = serializationManager.Serialize(obj);
			try
			{
				Log.Logger.Error<T>("{@data}", obj);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="obj"></param>
		public void LogInfo<T>(T obj)
		{
			var data = serializationManager.Serialize(obj);
			try
			{
				Log.Logger.Information<T>("{@data}", obj);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		public void LogWarning<T>(T obj)
		{
			var data = serializationManager.Serialize(obj);
			try
			{
				Log.Logger.Warning<T>("{@data}", obj);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		public void LogFatal<T>(T obj)
		{
			var data = serializationManager.Serialize(obj);
			try
			{
				Log.Logger.Fatal<T>("{@data}", obj);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}

		public void LogDebug<T>(T obj)
		{
			var data = serializationManager.Serialize(obj);
			try
			{
				Log.Logger.Debug<T>("{@data}", obj);
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
		}
	}
}
