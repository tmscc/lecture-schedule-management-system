﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LectureScheduleManagementSystem.Logging
{
    /// <summary>
    /// 
    /// </summary>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class SerializationManager : ISerializationManager
    {
        private readonly JsonSerializerSettings settings;

        public SerializationManager()
        {
            this.settings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            this.settings.Converters.Add(new StringEnumConverter());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input"></param>
        /// <returns></returns>
        public T Deserialize<T>(string input)
        {
            return JsonConvert.DeserializeObject<T>(input, this.settings);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, this.settings);
        }
    }
}
