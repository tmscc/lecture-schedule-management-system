﻿using System.Collections.Generic;
using System.IO;
using System.Web.UI;

namespace LectureScheduleManagementSystem.FileFormats
{
    /// <summary>
    /// 
    /// </summary>
    public class HtmlFileWriter : HtmlTextWriter
    {
        private ICollection<string> cssClasses;

        public HtmlFileWriter(TextWriter writer)
            : base(writer)
        {
            cssClasses = new List<string>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cssClass"></param>
        public void AddCssClass(string cssClass)
        {
            if (!cssClasses.Contains(cssClass))
            {
                cssClasses.Add(cssClass);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void AddCssClassAttribute()
        {
            AddAttribute(HtmlTextWriterAttribute.Class, string.Join(" ", cssClasses));
            cssClasses.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tagKey"></param>
        public void RenderBeginTag(HtmlTextWriterTag tagKey)
        {
            AddCssClassAttribute();
            base.RenderBeginTag(tagKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tagName"></param>
        public void RenderBeginTag(string tagName)
        {
            AddCssClassAttribute();
            base.RenderBeginTag(tagName);
        }
    }
}
