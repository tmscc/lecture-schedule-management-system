﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LectureScheduleManagementSystem.Helpers
{
	public static class StringHelpers
	{
		public static string RandomString(int length)
		{
			StringBuilder stringBuilder = new StringBuilder();
			Random random = new Random();

			for (int i = 0; i < length; i++)
			{
				double flt = random.NextDouble();
				int shift = Convert.ToInt32(Math.Floor(25 * flt));
				char letter = Convert.ToChar(shift + 65);
				stringBuilder.Append(letter);
			}

			return stringBuilder.ToString();
		}

		public static string ConvertStringArrayToString(string[] strings)
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach(var str in strings)
			{
				stringBuilder.Append(str + " ");
			}
			return stringBuilder.ToString().Trim();
		}

		public static string ConvertStringArrayToString(List<string> strings)
		{
			return ConvertStringArrayToString(strings.ToArray());
		}
	}
}
