﻿using System;
using System.Text;

namespace LectureScheduleManagementSystem.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	public static class ExceptionHelpers
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ex"></param>
		/// <returns></returns>
		public static string BuildMessage(Exception ex)
		{
			StringBuilder builder = new StringBuilder();

			builder.Append(ex.Message);

			Exception inner = ex;
			while(inner.InnerException != null)
			{
				inner = inner.InnerException;
			}

			builder.Append(inner.Message);

			return builder.ToString();
		}
	}
}
