﻿using LectureScheduleManagementSystem.FileFormats;
using LectureScheduleManagementSystem.Models;
using System.Collections.Generic;

namespace LectureScheduleManagementSystem
{
	public class CustomDialogResult
	{
		private bool? result;

		public bool? Result { get { return result; } set { result = !value; } }

		public List<ExcelColumn> Columns { get; set; }

		public ExcelFileWriter Writer { get; set; }
	}
}
