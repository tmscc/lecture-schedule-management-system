﻿using LectureScheduleManagementSystem.ViewModels;
using LectureScheduleManagementSystem.Views;
using System;
using System.Windows;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;

namespace LectureScheduleManagementSystem
{
    public class ApplicationController
    {
        private static ApplicationController controller;
        public static ApplicationController GetInstance()
        {
            if (controller == null)
            {
                controller = new ApplicationController();
            }

            return controller;
        }

        public void OpenWindow(ApplicationWindow window, ViewModelBase vm, bool normalWindow)
        {
            switch (window)
            {
                case ApplicationWindow.AddDialog:
                    CreateAndOpenWindow(new AddDialog(), vm, normalWindow);
                    break;
                case ApplicationWindow.EditDialog:
                    CreateAndOpenWindow(new EditDialog(), vm, normalWindow);
                    break;
                case ApplicationWindow.DeleteDialog:
                    CreateAndOpenWindow(new DeleteDialog(), vm, normalWindow);
                    break;
                case ApplicationWindow.MainWindow:
                    CreateAndOpenWindow(new MainWindow(), vm, normalWindow);
                    break;
                default:
                    break;
            }
        }

        private void CreateAndOpenWindow(Window window, ViewModelBase vm, bool normalWindow)
        {
            window.DataContext = vm;
            if (vm.CloseAction == null)
            {
                vm.CloseAction = new Action(() =>
                {
                    window.Close();
                });
            }
            if (vm.ShowPopupMessage == null)
            {
                vm.ShowPopupMessage = new Action<string, string>((text, header) => MessageBox.Show(text, header));
            }
            if (vm.ShowConfirmMessage == null)
            {
                vm.ShowConfirmMessage = new Func<string, string, bool>((text, header) => MessageBox.Show(text, header, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes);
            }
            if (vm.ShowOpenFileDialog == null)
            {
                vm.ShowOpenFileDialog = new Func<string, bool, string, string[]>(OpenFileDialogResult);
            }
            if (vm.ShowFolderBrowserDialog == null)
            {
                vm.ShowFolderBrowserDialog = new Func<Environment.SpecialFolder, bool, string>(OpenFolderBrowserDialog);
            }

            if (normalWindow)
            {
                window.Show();
            }
            else
            {
                window.ShowDialog();
            }
        }

        private string[] OpenFileDialogResult(string filter, bool multiselect, string initialFolder)
        {
            string[] fileNames = Array.Empty<string>();

            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();
            fileDialog.Filter = filter;
            fileDialog.Multiselect = multiselect;
            if (!string.IsNullOrEmpty(initialFolder))
            {
                fileDialog.InitialDirectory = initialFolder;
            }
            if (fileDialog.ShowDialog() == true)
            {
                fileNames = fileDialog.FileNames;
            }

            return fileNames;
        }

        private string OpenFolderBrowserDialog(Environment.SpecialFolder rootFolder, bool showNewFolderButton)
        {
            var fb = new FolderBrowserDialog
            {
                RootFolder = rootFolder,
                ShowNewFolderButton = showNewFolderButton
            };
            fb.ShowDialog();

            return fb.SelectedPath;
        }
    }
}
