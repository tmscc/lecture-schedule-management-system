﻿namespace LectureScheduleManagementSystem.Models
{
	/// <summary>
	/// 
	/// </summary>
	public class CellCoordinates
	{
		/// <value></value>
		public int Row { get; set; }

		/// <value></value>
		public int Column { get; set; }

		public CellCoordinates(int row, int column)
		{
			Row = row;
			Column = column;
		}
	}
}
