﻿using System;
using System.Collections.Generic;

namespace LectureScheduleManagementSystem.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Subject
    {
        /// <value></value>
        public string Name { get; set; }

        /// <value></value>
        public List<string> LecturerNames { get; set; }

        /// <value></value>
        public List<SubjectCell> SubjectCells { get; set; }

        /// <value></value>
        public Dictionary<DateTime, int> DateCells { get; set; }

        /// <value></value>
		public CellCoordinates CellCoordinates { get; set; }
	}
}
