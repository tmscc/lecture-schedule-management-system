﻿using System;

namespace LectureScheduleManagementSystem.Models
{
	public class SubjectCell : BaseModel
	{
		public bool IsDirty { get; set; }

		private DateTime date;
		public DateTime Date
		{
			get { return date; }
			set
			{
				date = value;
				IsDirty = true;
				OnPropertyChanged(nameof(Date));
			}
		}

		private string valueOfCell;
		public string ValueOfCell
		{
			get { return valueOfCell; }
			set
			{
				valueOfCell = value;
				IsDirty = true;
				OnPropertyChanged(nameof(ValueOfCell));
			}
		}

		private DateTime startingHour;
		public DateTime StartingHour
		{
			get { return startingHour; }
			set
			{
				startingHour = value;
				IsDirty = true;
				OnPropertyChanged(nameof(StartingHour));
			}
		}

		private DateTime endingHour;
		public DateTime EndingHour
		{
			get { return endingHour; }
			set
			{
				endingHour = value;
				IsDirty = true;
				OnPropertyChanged(nameof(EndingHour));
			}
		}

		//public DateTime Date { get; set; }

		//public string ValueOfCell { get; set; }

		//public DateTime StartingHour { get; set; }

		//public DateTime EndingHour { get; set; }

		public CellCoordinates CellCoordinates { get; set; }

		public int HoursPerDay { get; set; }

		public double MinutesPerDay { get; set; }

		public int ContactHours { get; set; }
	}
}
