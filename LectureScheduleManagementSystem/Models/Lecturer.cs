﻿
namespace LectureScheduleManagementSystem.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Lecturer
    {
        /// <value></value>
        public string Name { get; set; }

        /// <value></value>
        public CellCoordinates CellCoordinates { get; set; }
    }
}
