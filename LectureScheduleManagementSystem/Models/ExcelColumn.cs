﻿using System.Collections.Generic;

namespace LectureScheduleManagementSystem.Models
{
	public class ExcelColumn
	{
		public string FilePath { get; set; }

		public Group Group { get; set; }

		public Lecturer Lecturer { get; set; }

		public Subject Subject { get; set; }
	}
}
