﻿using System.Collections;
using System.ComponentModel;
using System.Diagnostics;

namespace CreatingInstaller
{
	/// <summary>
	/// 
	/// </summary>
	[RunInstaller(true)]
	public partial class Installer : System.Configuration.Install.Installer
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="savedState"></param>
		public override void Install(IDictionary savedState)
		{
			base.Install(savedState);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="savedState"></param>
		public override void Rollback(IDictionary savedState)
		{
			base.Rollback(savedState);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="savedState"></param>
		public override void Commit(IDictionary savedState)
		{
			base.Commit(savedState);
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="savedState"></param>
		public override void Uninstall(IDictionary savedState)
		{
			Process application = null;

			foreach (var process in Process.GetProcesses())
			{
				if (!process.ProcessName.ToLower().Contains("vum"))
				{
					continue;
				}

				application = process;
				break;
			}

			if (application != null && application.Responding)
			{
				application.Kill();
				base.Uninstall(savedState);
			}
		}
	}
}