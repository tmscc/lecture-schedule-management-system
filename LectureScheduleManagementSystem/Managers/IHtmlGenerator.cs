﻿using LectureScheduleManagementSystem.Models;
using System.Collections.Generic;

namespace LectureScheduleManagementSystem.Managers
{
	interface IHtmlGenerator : System.IDisposable
	{
		string Generate(List<ExcelColumn> columns, string filePath);
	}
}
