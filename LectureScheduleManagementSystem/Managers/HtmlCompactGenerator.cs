﻿using LectureScheduleManagementSystem.Extensions;
using LectureScheduleManagementSystem.FileFormats;
using LectureScheduleManagementSystem.Helpers;
using LectureScheduleManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.UI;

namespace LectureScheduleManagementSystem.Managers
{
	public class HtmlCompactGenerator : IHtmlGenerator
	{
		private StringWriter stringWriter = new StringWriter();
		private HtmlFileWriter writer;

		public HtmlCompactGenerator()
		{
			writer = new HtmlFileWriter(stringWriter);
		}

		public void RenderHead()
		{
			//Head start
			writer.RenderBeginTag(HtmlTextWriterTag.Head);
			writer.AddAttribute("name", "viewport");
			writer.AddAttribute("content", "width=device-width, initial-scale=1, shrink-to-fit=no");
			writer.RenderBeginTag(HtmlTextWriterTag.Meta);
			writer.RenderEndTag();

			//Bootstrap start
			writer.AddAttribute("rel", "stylesheet");
			writer.AddAttribute("href", "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css");
			writer.RenderBeginTag(HtmlTextWriterTag.Link);
			writer.RenderEndTag();

			writer.AddAttribute("src", "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js");
			writer.RenderBeginTag(HtmlTextWriterTag.Script);
			writer.RenderEndTag();

			writer.AddAttribute("src", "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js");
			writer.RenderBeginTag(HtmlTextWriterTag.Script);
			writer.RenderEndTag();

			writer.AddAttribute("src", "https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js");
			writer.RenderBeginTag(HtmlTextWriterTag.Script);
			writer.RenderEndTag();
			//Bootstrap end

			writer.AddAttribute("rel", "icon");
			writer.AddAttribute("href", "https://vum.bg/wp-content/uploads/2017/08/unnamed.png");
			writer.AddAttribute("sizes", "32x32");
			writer.RenderBeginTag(HtmlTextWriterTag.Link);
			writer.RenderEndTag();

			writer.RenderBeginTag(HtmlTextWriterTag.Style);
			writer.Write(".clickable:hover{cursor: pointer;}");
			writer.RenderEndTag();

			writer.AddAttribute("charset", "UTF-8");
			writer.RenderBeginTag(HtmlTextWriterTag.Meta);
			writer.RenderEndTag();
			writer.RenderBeginTag(HtmlTextWriterTag.Title);
			writer.Write("VUM Schedule");
			writer.RenderEndTag();
			writer.RenderEndTag();
			//Head end
		}

		public void RenderBody(List<ExcelColumn> columns)
		{
			if (columns == null)
			{
				throw new ArgumentNullException("Columns parameter in RenderBody is null");
			}

			List<string> groupNames = new List<string>();
			foreach (var column in columns)
			{
				foreach (string groupName in column.Group.Names)
				{
					groupNames.Add(groupName.Trim());
				}
			};
			groupNames = groupNames.Distinct().ToList();

			List<string> monthNames = new List<string>();
			foreach (var item in columns)
			{
				foreach (var subject in item.Subject.SubjectCells)
				{
					monthNames.Add(subject.Date.ToString("MMMM", CultureInfo.CurrentCulture));
				}
			}
			monthNames = monthNames.Distinct().ToList();
			//Body start
			writer.RenderBeginTag(HtmlTextWriterTag.Body);
			//Card start
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "card");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "card-header");
			writer.RenderBeginTag(HtmlTextWriterTag.H4);
			writer.Write("VUM Schedule");
			writer.RenderEndTag();

			//Card body start
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "card-body");
			writer.RenderBeginTag(HtmlTextWriterTag.Div);
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "accordion");
			writer.AddAttribute(HtmlTextWriterAttribute.Id, "mainAccordion");
			writer.RenderBeginTag(HtmlTextWriterTag.Div); //Start accordion

			foreach (var item in groupNames)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "card");
				writer.RenderBeginTag(HtmlTextWriterTag.Div); //Start card

				//Start card header
				string generatedTarget = StringHelpers.RandomString(20);
				string generatedId = StringHelpers.RandomString(20);
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "card-header clickable mb-0");
				writer.AddAttribute("data-toggle", "collapse");
				writer.AddAttribute("data-target", "#" + generatedTarget);
				writer.AddAttribute("aria-expanded", "true");
				writer.AddAttribute("aria-controls", generatedTarget);
				writer.AddAttribute(HtmlTextWriterAttribute.Id, generatedId);
				writer.RenderBeginTag(HtmlTextWriterTag.Div);

				writer.AddAttribute(HtmlTextWriterAttribute.Class, "collapsed");
				writer.AddAttribute(HtmlTextWriterAttribute.Style, "font-size: 25px");
				writer.RenderBeginTag("text");
				writer.Write(item);
				writer.RenderEndTag();
				writer.RenderEndTag(); //End card header

				writer.AddAttribute(HtmlTextWriterAttribute.Id, generatedTarget);
				writer.AddAttribute(HtmlTextWriterAttribute.Class, "collapse card");
				writer.AddAttribute(HtmlTextWriterAttribute.Style, "margin:25px");
				writer.AddAttribute("aria-labelledby", generatedId);
				writer.AddAttribute("data-parent", "#mainAccordion");
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
				//Calendar start

				writer.AddAttribute(HtmlTextWriterAttribute.Class, "card-header");
				writer.RenderBeginTag("nav"); //Start nav

				writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav nav-tabs");
				writer.AddAttribute(HtmlTextWriterAttribute.Id, "nav-tab");
				writer.AddAttribute("role", "tablist");
				writer.RenderBeginTag(HtmlTextWriterTag.Div); //Start months

				foreach (var month in monthNames)
				{
					string navName = $"nav-{month}";
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "nav-item nav-link");
					writer.AddAttribute("data-toggle", "tab");
					writer.AddAttribute(HtmlTextWriterAttribute.Href, "#" + navName + generatedTarget);
					writer.AddAttribute("aria-controls", navName);
					writer.AddAttribute(HtmlTextWriterAttribute.Id, navName + "-tab");
					writer.AddAttribute("role", "tab");
					writer.RenderBeginTag("a");
					writer.Write(month);
					writer.RenderEndTag();
				}

				writer.RenderEndTag(); //End months

				writer.RenderEndTag(); //Nav end

				writer.AddAttribute(HtmlTextWriterAttribute.Class, "tab-content card-body");
				writer.AddAttribute(HtmlTextWriterAttribute.Id, "nav-tabContent");
				writer.RenderBeginTag(HtmlTextWriterTag.Div); //Start TabContent

				foreach (var month in monthNames)
				{
					List<SubjectCell> filtered = new List<SubjectCell>();

					foreach (var filteredItem in columns)
					{
						filteredItem.Subject.SubjectCells = filteredItem.Subject.SubjectCells.OrderBy(x => x.Date).ToList();
						if (filteredItem.Group.Names.Contains(item))
						{
							List<SubjectCell> subjectCells = filteredItem.Subject.SubjectCells.Where(x => x.Date.ToString("MMMM").Equals(month)).ToList();
							foreach (var subjectCell in subjectCells)
							{
								filtered.Add(subjectCell);
							}
						}
					}

					string navName = $"nav-{month}";
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "tab-pane fade");
					writer.AddAttribute(HtmlTextWriterAttribute.Id, navName + generatedTarget);
					writer.AddAttribute("aria-labelledby", navName + "-tab");
					writer.AddAttribute("role", "tabpanel");
					writer.RenderBeginTag(HtmlTextWriterTag.Div);

					//Start table
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "table table-bordered table-hover");
					writer.RenderBeginTag(HtmlTextWriterTag.Table);

					writer.RenderBeginTag(HtmlTextWriterTag.Thead);
					writer.RenderBeginTag(HtmlTextWriterTag.Tr);

					foreach (var dayOfTheWeek in Constants.DaysOfTheWeek)
					{
						writer.AddAttribute(HtmlTextWriterAttribute.Scope, "col");
						writer.AddAttribute(HtmlTextWriterAttribute.Class, "bg-primary text-white");
						writer.RenderBeginTag(HtmlTextWriterTag.Th);
						writer.Write(dayOfTheWeek);
						writer.RenderEndTag();
					}

					writer.RenderEndTag(); //tr
					writer.RenderEndTag(); //thead

					writer.RenderBeginTag(HtmlTextWriterTag.Tbody);
					Func<DateTime, int> weekProjector =
						 d => CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(
						 d,
						 CalendarWeekRule.FirstFullWeek,
						 DayOfWeek.Sunday);

					IOrderedEnumerable<SubjectCell> list = filtered.OrderBy(x => x.Date);
					var groups = list.GroupBy(d => d.Date.GetWeekOfYear()).Select(g => new
					{
						Week = g.Key,
						Dates = g.ToArray(),
						Count = g.Count()
					});

					foreach (var group in groups)
					{
						writer.RenderBeginTag(HtmlTextWriterTag.Tr);
						foreach (var date in group.Dates)
						{
							if (!string.IsNullOrEmpty(date.ValueOfCell))
							{
								writer.RenderBeginTag(HtmlTextWriterTag.Td);
								writer.Write(date.ValueOfCell);
								writer.RenderEndTag();
							}
						}
						writer.RenderEndTag();
					}

					writer.RenderEndTag(); //tbody

					//End table
					writer.RenderEndTag();

					writer.RenderEndTag();
				}

				writer.RenderEndTag(); //End TabContent

				//Calendar end
				writer.RenderEndTag();

				writer.RenderEndTag(); //End card
			}

			writer.RenderEndTag(); //End accordion

			//Card body end
			writer.RenderEndTag();
			//Card end
			writer.RenderEndTag();
			//Body end
			writer.RenderEndTag();
		}

		public string Generate(List<ExcelColumn> columns, string filePath)
		{
			writer.RenderBeginTag("!DOCTYPE html");
			writer.RenderBeginTag(HtmlTextWriterTag.Html);
			RenderHead();
			RenderBody(columns);
			writer.RenderEndTag();
			System.IO.File.WriteAllText(filePath + ".html", stringWriter.ToString());

			return stringWriter.ToString();
		}

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// TODO: dispose managed state (managed objects).
				}

				// TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
				// TODO: set large fields to null.

				disposedValue = true;
			}
		}

		// TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
		// ~HtmlCompactGenerator()
		// {
		//   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
		//   Dispose(false);
		// }

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
			// TODO: uncomment the following line if the finalizer is overridden above.
			// GC.SuppressFinalize(this);
		}
		#endregion

	}
}
