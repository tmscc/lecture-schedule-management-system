# Lecture Schedule Management System (LSMS)

## Description
LSMS is a software aimed for VUM's Student's Affairs office. It is a desktop application that is being developed by a group of 4 students of the university. The main purpose of this software is to ease the process of creating the schedule of an the academic year at VUM including additional features like counting the contact hours, the total hours per each lecturer, displaying warnings etc.

## Built with
* Visual Studio

## Status
*In progress*  
The project was proposed one month ago (14.02.2020) and requires more time to be finished. 

## Authors
* Eneada Sulaj
* Iva Tsaneva
* Peter Yordanov
* Velislava Petrova

## Inspiration
This project was suggested by Yordanka Budinova to the groups of second-year software engineering students. After a thorough discussion the group of four members (later named as "Team Argo") decided to develop the software for the schedule application. The idea was welcomed by the Student's Affairs office who are also the product owners.